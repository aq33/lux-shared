#pragma once

#include <lux_shared/common.hpp>
#include <lux_shared/net/common.hpp>
#include <lux_shared/net/serial.hpp>
#include <lux_shared/map.hpp>
#include <lux_shared/entity.hpp>

#pragma pack(push, 1)

struct NetSsInit {
    Arr<char, SERVER_NAME_LEN> name;
    U16                   tick_rate;
};

struct NetSsTick {
    struct Entities {
        //@TODO we probably don't need ECS here
        struct Health {
            struct Stat {
                StrBuff name;
                F32 val;
                F32 change;
            };
            DynArr<Stat> stats;
        };
        struct Inventory {
            DynArr<EntityId> items;
        };

        using Pos  = EntityVec;
        using Name = Arr<char, 32>;

        IdMap<EntityId, Pos>       pos;
        IdMap<EntityId, Name>      name;
        IdMap<EntityId, Health>    health;
        IdMap<EntityId, Inventory> inventory;
        IdMap<EntityId, BlockId>   voxel_item;
        IdMap<EntityId, F32>       stack;
    };
    ///1 is expressed as midday, -1 is expressed as midnight
    F32      day_cycle;
    EntityId player_id;
    Entities entities;
};

struct NetSsSgnl {
    struct ChunkMeshLoad {
        struct Mesh {
            DynArr<BlockFace> faces;
        };
        VecMap<ChkPos, Mesh> meshes;
    };
    struct ChunkMeshUpdate {
        struct Mesh {
            DynArr<Uns>       removed_faces;
            DynArr<BlockFace> added_faces;
        };
        VecMap<ChkPos, Mesh> meshes;
    };
    enum Tag : U8 {
        CHUNK_MESH_LOAD = 0x00,
        CHUNK_MESH_UPDATE,
        TAG_MAX,
    } tag = TAG_MAX;

    ChunkMeshLoad   chunk_mesh_load;
    ChunkMeshUpdate chunk_mesh_update;
};

struct NetCsTick {
    EntityVec move_dir;
    Vec2F     yaw_pitch;
    bool      is_moving;
    bool      is_jumping;
};

struct NetCsInit {
    struct {
        U8 major;
        U8 minor;
        U8 patch;
        static_assert(sizeof(major) == sizeof(NET_VERSION_MAJOR));
        static_assert(sizeof(minor) == sizeof(NET_VERSION_MINOR));
        static_assert(sizeof(patch) == sizeof(NET_VERSION_PATCH));
    } net_ver;
    Arr<char, CLIENT_NAME_LEN> name;
};

struct NetCsSgnl {
    struct ChunkMeshRequest {
        VecSet<ChkPos> requests;
    };
    struct ChunkMeshUnload {
        VecSet<ChkPos> chunks;
    };
    enum DoAction : U8 {
        BREAK_BLOCK,
        PLACE_BLOCK,
    };
    struct DoInteraction {
        enum Tag {
            EAT
        } tag;
        EntityId id;
    };
    enum Tag : U8 {
        CHUNK_MESH_REQUEST = 0x00,
        CHUNK_MESH_UNLOAD,
        DO_ACTION,
        DO_INTERACTION,
        TAG_MAX,
    } tag = TAG_MAX;

    ChunkMeshRequest chunk_mesh_request;
    ChunkMeshUnload  chunk_mesh_unload;
    DoAction         do_action;
    DoInteraction    do_interaction;
};

#pragma pack(pop)
