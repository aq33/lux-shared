#pragma once

#include <utility>
#include <type_traits>

#pragma pack(push, 1)
template<typename T>
struct Uninit {
    std::aligned_storage_t<sizeof(T), alignof(T)> data;

    template<typename... Args>
    void init(Args&& ...args) {
        new ((T*)&data) T(std::forward<Args>(args)...);
    }

    void deinit() {
        ((T*)&data)->~T();
    }

    T& operator*() const {
        return (T&)data;
    }

    T* operator->() const {
        return (T*)&data;
    }

    Uninit() = default;
    Uninit(Uninit&& that) = delete;
    Uninit& operator=(Uninit&& that) = delete;
};
#pragma pack(pop)
