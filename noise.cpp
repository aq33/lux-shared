#include <noise/Simplex.h>
//
#include <lux_shared/noise.hpp>

//@TODO add random seed

F32 noise(F32 seed) {
    return Simplex::noise(seed);
}

F32 noise(Vec2F seed) {
    return Simplex::noise(seed);
}

F32 noise(Vec3F seed) {
    return Simplex::noise(seed);
}

Vec2F noise_deriv(F32 seed) {
    return Simplex::dnoise(seed);
}

Vec3F noise_deriv(Vec2F seed) {
    return Simplex::dnoise(seed);
}

Vec4F noise_deriv(Vec3F seed) {
    return Simplex::dnoise(seed);
}

template<typename T>
F32 noise_fbm(T seed, Uns octaves)
{
    F32 sum = 0.f;
    F32 amp = 0.5f;
    for(Uns i = 0; i < octaves; ++i) {
        sum  += noise(seed) * amp;
        ///we don't multiply exactly by 2, because that makes the octaves
        ///overlap
        seed *= 1.9f;
        amp  *= 0.5f;
    }
    return sum / (1.f - 2.f * amp);
}

template<typename T1, typename T2>
T1 noise_deriv_fbm(T2 seed, Uns octaves) {

    T1 sum = T1(0.f);
    F32 amp = 0.5f;
    for(Uns i = 0; i < octaves; ++i) {
        sum  += noise_deriv(seed) * amp;
        ///we don't multiply exactly by 2, because that makes the octaves
        ///overlap
        seed *= 1.9f;
        amp  *= 0.5f;
    }
    //@TODO is this correct?
    return sum / (1.f - 2.f * amp);
}

template F32 noise_fbm<F32>(  F32   seed, Uns octaves);
template F32 noise_fbm<Vec2F>(Vec2F seed, Uns octaves);
template F32 noise_fbm<Vec3F>(Vec3F seed, Uns octaves);
template Vec2F noise_deriv_fbm<Vec2F, F32>(  F32   seed, Uns octaves);
template Vec3F noise_deriv_fbm<Vec3F, Vec2F>(Vec2F seed, Uns octaves);
template Vec4F noise_deriv_fbm<Vec4F, Vec3F>(Vec3F seed, Uns octaves);
